library(readr)
X7COM1079introsurvey <- read_csv("X7COM1079introsurvey.csv", col_types = cols(X1 = col_skip()))
levels<-X7COM1079introsurvey[,c(6,7,8,9,10)]
res <- cor(levels,use="pairwise.complete.obs")
res
plot(jitter(levels$GitLevel,1),jitter(levels$KanbanLevel,1),pch=5,xlab="Proficiency in GIT",ylab="Proficiency in Kanban")
