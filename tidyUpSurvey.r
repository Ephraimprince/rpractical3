library(readr)
X7COM1079introsurvey <- read_csv("7COM1079introsurvey.csv")
names(X7COM1079introsurvey)[6] <- "GitLevel"
names(X7COM1079introsurvey)[7] <- "SpreadsheetLevel"
names(X7COM1079introsurvey)[8] <- "StatsLevel"
names(X7COM1079introsurvey)[9] <- "RLevel"
names(X7COM1079introsurvey)[10] <- "KanbanLevel"
names(X7COM1079introsurvey)[11] <- "BestChartPercent"
names(X7COM1079introsurvey)[12] <- "BestChartHistoric"
names(X7COM1079introsurvey)[13] <- "BestChartRelationship"
names(X7COM1079introsurvey)[14] <- "CSTopicsOrderPref"
names(X7COM1079introsurvey)[15] <- "PValueQ"
write.csv(X7COM1079introsurvey,'survey.csv')